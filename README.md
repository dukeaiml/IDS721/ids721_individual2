# work_with_redis
Using Redis in a Simple Rust web service


### Overview
In project 2, the purpose is to build a functional Web Microservice in Rust based on Docker or other similar platforms (Kubernetes Deployments provide a higher-level abstraction for managing the deployment and scaling of applications in a cluster, making it easier to automate and manage the process). I created a simple Microservice to interact with redis, which can store/remove/check blocked address.

This Microservice has multiple routes:

A. type: "/block" that blocks the targeted address and stores it in redis

B. type: "/release" that removes the blocked address from redis

C. type: "/check" that checks the blocked addresses
## Preparation
### 1. Containerization: Setup virtual environment
A virtual environment is a tool that helps to keep dependencies required by different projects separate by creating isolated python virtual environments for them. 
* Type: `python3 -m venv env` and `source env/bin/activate`

### 2. Instal Rust
* Type: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh` and then `source "$HOME/.cargo/env"`

### 3. create new project
* Type: `cargo new (project name)` (my Eg: `cargo new src`)
* Create main.rs and lib.rs for the src project
* Cargo build: it is a command in the Rust programming language that is used to compile a Rust project. It compiles the project's source code and its dependencies, and produces an executable binary file. The `cargo build` command can be run from the root directory of the project.
* Set up Cargo.toml to determine the dependencies and build configuration of the project.

![Alt text](image.png)

* Set up Dockerfile for APP, there we have 2 services including our web service and redis. We need to setup a yml file to startup 2 services and set the connection(to redis) host name to "redis" which is the service name. Also change the listening host to 0.0.0.0:port for accessing from outer addresses

![Alt text](image-1.png)

![Alt text](image-2.png)


* Create a Makefile: it is a special file that lists a set of rules for compiling a project. These rules include targets, which can be an action make needs to take or the files/objects make will need to build, and the commands that need to be run in order to build that target. 

### Test The Service:

add to block list:
`
curl -d "hell.com" -X POST 127.0.0.1:1337/block
`
![Alt text](image-4.png)

![Alt text](image-3.png)

remove from block list:
`
curl -d "hell.com" -X POST 127.0.0.1:1337/release
`
![Alt text](image-8.png)

![Alt text](image-7.png)

![Alt text](image-9.png)

check block list:
`
curl -d "hell.com" -X POST 127.0.0.1:1337/check
`
![Alt text](image-6.png)

![Alt text](image-5.png)

### CICD
Use docker:cli as image and docker:dind service to run docker in the docker image

![Alt text](image-10.png)

![Alt text](image-11.png)

### TODOs:

- [ ] add more test
- [ ] edit names
- [ ] remove unwrap and add logger
- [ ] connection pool
