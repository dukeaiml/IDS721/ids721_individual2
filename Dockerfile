# 设置基础镜像为 Rust 官方提供的最新版本
FROM rust:latest as builder

# 设置工作目录
WORKDIR /usr/src/work_with_redis

# 将当前目录下的所有文件复制到容器的工作目录中
COPY . .

# 构建 Rust 项目
RUN cargo build --release

# 使用另一个基础镜像开始新的构建阶段
FROM ubuntu:latest

# 设置工作目录
WORKDIR /usr/local/bin/

# 从builder阶段复制编译后的可执行文件到容器中
COPY --from=builder /usr/src/work_with_redis/target/release/work_with_redis /usr/local/bin/

# 从builder阶段复制配置文件到容器中
COPY --from=builder /usr/src/work_with_redis/Config.json /usr/local/bin/

# 打印出可执行文件目录下的所有文件
RUN ls -al /usr/local/bin/

# 暴露应用程序的端口
EXPOSE 1337

# 运行应用程序
CMD ["work_with_redis"]
